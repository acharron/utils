import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.xml.bind.DatatypeConverter;

/**
 * パスワード保存と確認の専用クラス
 * 
 * 参考：
 * https://crackstation.net/hashing-security.htm
 * https://github.com/defuse/password-hashing/blob/master/PasswordStorage.java
 * 
 * @author シャロン アーサー
 */
public class SecurityUtils {
	
	/**
	 * ハッシュのエラーに関する例外
	 */
	@SuppressWarnings("serial")
	static public class InvalidHashException extends Exception {
        public InvalidHashException(String message) {
            super(message);
        }
        public InvalidHashException(String message, Throwable source) {
            super(message, source);
        }
    }
	
	// cf https://stackoverflow.com/questions/19348501/pbkdf2withhmacsha512-vs-pbkdf2withhmacsha1
	// この変数は変更禁止
	public static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA512";
	
	// この変数を変更しても、前保存されたハッシュに影響がない：変更可能
	// NB : BYTESの長さは 64 まで。（SHA512で最長512ビットのハッシュを計算できるから）
	public static final int SALT_BYTES = 32;  // 256 ビット
	public static final int HASH_BYTES = 32;  // 256 ビット
    public static final int PBKDF2_ITERATIONS = 6000;
	
    // この変数はフォーマットを指定しているので変更禁止
    // フォーマット:  ハッシュ回数:salt:ハッシュ
    public static final int HASH_SECTIONS = 3; // ハッシュに部分の数
    public static final int ITERATION_INDEX = 0;
    public static final int SALT_INDEX = 1;
    public static final int HASH_INDEX = 2;
    
    
    public static String createHashPassword(String rawPassword) 
    		throws NoSuchAlgorithmException, InvalidKeySpecException{
    	return createHashPassword(rawPassword.toCharArray());
    }
    
    /**
     * 新規saltを作成してからパスワードをハッシュする。
     * 
     * DBにはひとつのStringでパスワードのハッシュとそのハッシュの計算に必要なパラメーターを保存する。
     * 将来てきに、本SecurityUtilsの変数 (バイト数・ハッシュ回数)を変更にしたいなら、すでにDBに保存
     * されているパスワードもハッシュを計算できるようにパラメーターが全部保存されてる。
     * 
     * @param rawPassword ユーザーが入力したパスワード
     * @return "ハッシュ回数:salt:ハッシュ" のフォーマットに従うString
     */
	public static String createHashPassword(char[] rawPassword) 
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		// 新規saltを作成する
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[SALT_BYTES];
		random.nextBytes(salt);
		
		byte[] hash = pbkdf2(rawPassword, salt, PBKDF2_ITERATIONS, HASH_BYTES);
		
		// フォーマット:  ハッシュ回数:salt:ハッシュ
		return PBKDF2_ITERATIONS + ":" + DatatypeConverter.printHexBinary(salt) + ":" + DatatypeConverter.printHexBinary(hash);
	}
	
	
	public static boolean isPasswordIdentical(String rawPassword, String goodHash) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidHashException {
		return isPasswordIdentical(rawPassword.toCharArray(), goodHash);
	}
	
	/**
	 * パスワードが一致しているか確認する。
	 * 
	 * @param rawPassword ユーザーが入力したパスワード
	 * @param goodHash DBに保存されているハッシュ (フォーマット:  ハッシュ回数:salt:ハッシュ)
	 * @return 一致すればTrue
	 */
	public static boolean isPasswordIdentical(char[] rawPassword, String goodHash) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidHashException {
		// フォーマット:  ハッシュ回数:salt:ハッシュ
        String[] params = goodHash.split(":");
        if (params.length != HASH_SECTIONS) {
            throw new InvalidHashException(
                "保存されたハッシュに変数の数は間違えている。"
            );
        }
        
        // 変数の取得（Stringにintを保存しているから読み込みの際、注意が必要）
        int iterations = 0;
        try {
            iterations = Integer.parseInt(params[ITERATION_INDEX]);
        } catch (NumberFormatException ex) {
            throw new InvalidHashException(
                "ハッシュ回数の読み込みに失敗しました。",
                ex
            );
        }

        if (iterations < 1) {
            throw new InvalidHashException(
                "変数回数は <1 となっています。"
            );
        }
        
        byte[] salt = DatatypeConverter.parseHexBinary(params[SALT_INDEX]);
        byte[] hash = DatatypeConverter.parseHexBinary(params[HASH_INDEX]);
		
		byte[] testHash = pbkdf2(rawPassword, salt, iterations, hash.length);
		
		return slowEquals(testHash, hash);
	}
	
    private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bytes)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
        return skf.generateSecret(spec).getEncoded();
    }
	
    private static boolean slowEquals(byte[] a, byte[] b) {
		int diff = a.length ^ b.length;
		for(int i = 0; i < a.length && i < b.length; i++)
		    diff |= a[i] ^ b[i];
		return diff == 0;
    }
}
