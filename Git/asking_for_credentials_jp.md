# Git - ログイン情報の保存方法

参考資料 

* (EN): [スタック・オーバーフロー : Is there a way to skip password typing when using https:// on GitHub?](https://stackoverflow.com/questions/5343068/is-there-a-way-to-skip-password-typing-when-using-https-on-github)
* (EN): [スタック・オーバーフロー : Remove credentials from Git](https://stackoverflow.com/questions/15381198/remove-credentials-from-git)
* (JP): [詳細 : Git 公式ガイド : 7.14章 Git のさまざまなツール - 認証情報の保存](https://git-scm.com/book/ja/v2/Git-%E3%81%AE%E3%81%95%E3%81%BE%E3%81%96%E3%81%BE%E3%81%AA%E3%83%84%E3%83%BC%E3%83%AB-%E8%AA%8D%E8%A8%BC%E6%83%85%E5%A0%B1%E3%81%AE%E4%BF%9D%E5%AD%98)


## Credential helpers の設定・説明

```bash
git config --global credential.helper "cache --timeout=120"  # パソコンキャッシュに2時間保存 (デフォールト15分間)
git config --global credential.helper manager                # Windows 認証方法 (推進)
git config --global credential.helper store                  # 自分の %HOME% (C:/Users/...../.git-credentials) に保存

# 現在の利用している config を確認する
git config credential.helper
```


| ヘルパータイプ | 説明                                                                                                                       |
|----------------|----------------------------------------------------------------------------------------------------------------------------|
| `cache`        | パソコンのキャッシュに保存して、 timeout が切れるまでパスワードの入力が不要                                                |
| `store`        | ローカルファイルで保存する。 **セキュリティー上ではよくない。** そのファイルを閲覧権限があるユーザーがパスワードを見れる！ |
| `manager`      | (推進) Windows のみ。 「資格情報マネージャー」を利用して、Windowsのユーザーアカウントでパスワードを管理する                |



## パスワードの入力を間違えて、もう一度入力はできないのでクローンが毎回失敗する！


`manager` の認証方法を使うと、初期パスワードの入力欄が表示される。しかし、その段階でパスワードを間違えても、入力欄が二度と表示されない。

というわけで、もう一度クローンなどを使ってみると、「認証失敗」となってしまう。

### Windowsの「資格情報マネージャー」でパスワード編集

`manager` は Windows のみの認証方法で、コントロールパネルから編集しなければならない。

Windows 7 の場合 :

* コントロールパネル
* 資格情報マネージャー

(表示されていなえれば、「コントロールパネル -> ユーザーアカウント」の画面に書いてあるはず)


Windows 10 の場合 :

* コントロールパネル
* ユーザーアカウント
* 資格情報マネージャー
* Windows 資格情報
* 汎用資格情報
