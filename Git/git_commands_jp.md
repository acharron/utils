# 様々な役に立つ Git コマンド

## 1つのリモート名に複数のプッシュ先のURLを設定

```bash
git remote set-url --add --push origin git://original/repo.git
git remote set-url --add --push origin git://another/repo.git
```

## リモートの方にブランチを削除

```bash
git push <remote_name> --delete <remote_branch>
```

## すでにプッシュされたファイルを後で `.gitignore` に追加

間違えてコミットされたファイルは、 `.gitignore` に存在しても、これからの変更は管理される。

リモートに削除して、これからのコミットに入れないように、以下のコマンドで直せる

```bash
git rm --cached <file>
// または (すべてのファイル)
git rm -r --cached .
git add -A
git commit -am "Remove ignored files"
git push
```

`rm --cached` で、「変更点が管理されているファイル」のリストを削除して、 `add -A` で `.gitignore`
に引っかからないファイルをもう一度追加する。よって、 `.gitignore` に引っかかったファイルだけが再追加
されずに、リモートから削除される
