# Git - Managing credentials

References

* (EN): [StackOverflow: Is there a way to skip password typing when using https:// on GitHub?](https://stackoverflow.com/questions/5343068/is-there-a-way-to-skip-password-typing-when-using-https-on-github)
* (EN): [StackOverflow : Remove credentials from Git](https://stackoverflow.com/questions/15381198/remove-credentials-from-git)

## Credential helpers

```bash
git config --global credential.helper "cache --timeout=120"     # Saved in the cache for 2 hours (defaults to 15 minutes)
git config --global credential.helper manager   # (Recommended) Saved using Windows Credentials
git config --global credential.helper store     # Stores permanently on a local file .git-credentials in %HOME%

# Checking which helper is currently in use
git config credential.helper
```


| Helper    | Explanation                                                                                         |
|-----------|-----------------------------------------------------------------------------------------------------|
| `cache`   | Saved in the computer cache. The password doesn't need to be typed again until the timeout expires. |
| `store`   | Saved on a local file, in plaintext! **Not very secure**                                            |
| `manager` | (Recommended) Windows only. Saved in "Windows Credentials" (managed for every Windows user account) |



## I typed the wrong password, and it's not asking again even if the login fails!

When using the `manager` helper, the password is stored by Windows, and the prompt to type it is displayed only once, 
even if the password is incorrect. 

Every next attempt to clone or login to a repository fails, and it seems there is no way to reset it.

### Changing the password in Windows Credentials

`manager` uses the Windows Credentials functionnality to save passwords, so you have to use the GUI to change them.

On Windows 7:

* Control Panel
* Manage Windows Credentials

On Windows 10:

* Control Panel
* User accounts
* Manage your credentials

(To check)
