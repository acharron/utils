# Commandes pour Git

## Avoir une remote qui push sur 2 URL en même temps

```bash
git remote set-url --add --push origin git://original/repo.git
git remote set-url --add --push origin git://another/repo.git
```

## Enlever une branche sur un repo en remote

```bash
git push <remote_name> --delete <remote_branch>
```

## Enlever un fichier du remote qu'on a rajouté au `.gitignore`

Et l'enlever totalement du remote aussi. Utile pour les fichiers de build générés localement.

```bash
git rm --cached <file>
// ou
git rm -r --cached .
git add .
git commit -am "Remove ignored files"
```
