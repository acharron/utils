# Comment lire un fichier local en Javascript (fetch)

Source : https://stackoverflow.com/a/46129280/4976476


```javascript
fetch("api_resources/tire_detail_template_dummy_data.html")
            .then(response => response.text())
            .then(text => htmlContainer.setHtml(text));
```
