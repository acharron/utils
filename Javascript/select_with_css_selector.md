# How to select with a CSS selector (to be more precise than getElementsByClass)

```javascript
var x = document.querySelector("a[target][name='aaa']");            // The first one
var elements = document.querySelectorAll("a[target][name='aaa']");  // All
```

Test page

```html
<!DOCTYPE html>
<html>
<head>
<style>
a[target] {
   background-color: yellow;
}
</style>
</head>
<body>
<a href="https://www.w3schools.com">w3schools.com</a>
<a href="http://www.disney.com" target="_blank">disney.com</a>
<a href="http://www.wikipedia.org" target="_top" name="bbb">wikipedia.org</a>
<br>

<button onclick="myFunction()">Try it</button>

<script>
function myFunction() {
   var x = document.querySelectorAll("a[target][name]");
   var i;
   for (i = 0; i < x.length; i++) {
       x[i].style.border = "10px solid red";
   }
}
</script>
</body>
</html>
```