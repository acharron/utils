# Gitlab Backup

## Path

Found in `/opt/gitlab/embedded/service/gitlab-rails/config/gitlab.yml`

Should be something like `/var/opt/gitlab/backups`

## Backup command

Cf <https://docs.gitlab.com/ee/raketasks/backup_restore.html>

```shell
sudo gitlab-rake gitlab:backup:create
```

## Copy to Windows NAS server

Reference : <http://wiki.linuxquestions.org/wiki/Copy_files_between_Linux_and_Windows>

```shell
# Check all the services available
smbclient -N -L 192.168.251.22       # -N for no password asked

# Make the mount directory
mkdir -p /mnt/kumamoto_nas/share     # We are accessing the share folder, so we mount it to share, for clarity

# Mount the folder in Linux
mount -o user="<domain>\<user>",password="<password>" //192.168.251.22/share /mnt/kumamoto_nas/share

# Success!
# We can now copy files and stuff as if it was part of the local machine
echo "aaa" > test.txt
cp test.txt /mnt/kumamoto_nas/share
```

## Remove old backups (but keep the last x)

See <https://stackoverflow.com/questions/25785/delete-all-but-the-most-recent-x-files-in-bash#25790>

```bash
echo "Removing old backups (keeping last $nb_backups) in $folder ..."
ls -p "$folder" | grep -v '/$' | sort -r | tail -n +$((nb_backups+1)) | xargs -I {} rm -- $folder/{}
echo "Done."
```

* `ls -p` shows the directory with the `/` suffix
* `grep -v '/$'` excludes the directories from the pipe (otherwise they would be counted by tail)
* `sort -r` sorts in reverse order (because I didn't want to use ls built-in functions, you never know)
* `tail -n +x` outputs all except the first x files
* `xargs` uses the tail output to rm
