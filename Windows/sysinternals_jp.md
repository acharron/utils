# Windows - Sysinternals デバッグ用のツール

Windows でプロセスなどをデバッグする際、あまり詳しい情報は取れないことが多い。
10年前から、 Sysinternals という会社がそのためのツールを開発して、無料でリリースしてた。
最近、Microsoft社に合併されて、サポートがまだ続けているみたい。

あまり知られていないツールみたいが、結構便利。残念ながら、英語しか見つからなかった。

公式ドキュメンテーションとダウンロードページ : 

* <https://docs.microsoft.com/en-us/sysinternals/downloads/>
* <https://live.sysinternals.com/Files/>

このフォルダーにもっとも役に立つ 3つのツールを `.zip` で置いてる。 (ダウンロードページにアクセスが出来ない場合)

## Handle

<https://docs.microsoft.com/en-us/sysinternals/downloads/handle>

「このファイルが何のプロセスに使われている？」

例えば、「プロセスで利用中のため、このファイルは削除できません。」というエラーがでる時、どのプロセスを終了するべきか調査できる！

## Process Explorer

<https://docs.microsoft.com/en-us/sysinternals/downloads/process-explorer>

タスクマネージャの「プロセス」タブより詳しい

## Process Monitor

<https://docs.microsoft.com/en-us/sysinternals/downloads/procmon>

このプロセスはどのレジストリーキーを触っているか、といったことを調査したい場合


## その他

公式ドキュメンテーションにはそれ以外色々なツールがあるので、大事なリソースだ
