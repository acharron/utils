# Windows - Sysinternals tools for debugging

When debugging for Windows, Sysinternals made some tools that are really useful.

All the documentation and the download page is here:

* <https://docs.microsoft.com/en-us/sysinternals/downloads/>
* <https://live.sysinternals.com/Files/>

`.zip` files are also included for the most useful tools (see below)

## Handle

<https://docs.microsoft.com/en-us/sysinternals/downloads/handle>

Which process uses the current file?

Very useful when getting a "File could not be deleted because it's opened by another process" error.

## Process Explorer

<https://docs.microsoft.com/en-us/sysinternals/downloads/process-explorer>

More complete version of the task manager.

## Process Monitor

<https://docs.microsoft.com/en-us/sysinternals/downloads/procmon>

Filter for registry keys, specific processes, etc


## Others

There are a lot of other utilities, it's a very useful resource.

