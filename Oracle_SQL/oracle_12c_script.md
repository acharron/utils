# Oracle 12c scripting

## Cannot create a user

cf http://www.dba-oracle.com/t_ora_65096_create_user_12c_without_c_prefix.htm

```sql
ALTER session SET "_ORACLE_SCRIPT"=true;

CREATE USER aaaaa
IDENTIFIED BY bbbbb
DEFAULT TABLESPACE USERS
TEMPORARY TABLESPACE TEMP
PROFILE DEFAULT
...
```