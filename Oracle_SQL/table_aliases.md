# Table aliases

## Quel est le problème avec cette requête ?

```sql
SELECT DISTINCT G.プラントコード
     ,G.定検年度回数
     , ...
 FROM PS_T_グループマスタ AS G
 LEFT JOIN PS_T_個人入所履歴情報 AS N ON G.グループコード = N.グループコード
WHERE G.定検年度回数 = 1
ORDER BY G.グループコード
```

**Il n'y a pas besoin de mettre `AS` derrière une définition de table !**

## Requête correcte

```sql
SELECT DISTINCT G.プラントコード
     ,G.定検年度回数
     , ...
 FROM PS_T_グループマスタ G
 LEFT JOIN PS_T_個人入所履歴情報 N ON G.グループコード = N.グループコード
WHERE G.定検年度回数 = 1
ORDER BY G.グループコード
```
