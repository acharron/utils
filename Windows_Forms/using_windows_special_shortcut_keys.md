# Handling special shortcuts keys events

In a Windows Form, clicking F10 makes the window focus to the menu bar.
The problem is that when opening a dialog form (`ShowDialog()`) with a key press that contained F10, the focus would not be set properly...

The way to handle this is by terminating the event on a key press (not pass it to the Form inner method)

```csharp
private void OnKeyDown(object sender, KeyEventArgs e)
{
    // SHIFT + F10
    if (e.Shift && e.KeyCode == Keys.F10)
    {
        e.Handled = true;
        // OR
        e.SuppressKeyPress = true;
        // (They have the same effect...)

        var input = new InputUserIdModal();
        input.ShowDialog(this);
        return;
    }
}
```
