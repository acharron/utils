# Radio buttons in Windows Forms (C#)

**designer.csのクラス**で、グループの各ボタンに

```cs
this.radioButton.Tag = 1; //グループの中のユーニックなID
this.radioButton.Click += new System.EventHandler(this.radiobuttons_Click);
```

を指定して、このようなEventHandlerを作る。

```cs
private void radiobuttons_Click(object sender, EventArgs e)
    {
      RadioButton newRadioButton = sender as RadioButton;

      // クリックされたボタンはもうすでに選択されているボタンなら、処理なし
      if (newRadioButton == currentRdbMaster) return;
      else
      {
        DialogResult res = Msg.ShowMessage(
                Msg.TYPE.QUESTION, "Q-Common-000-0006",
                MessageBoxButtons.YesNo, "マスタ");
        // キャンセルの場合、処理なし
        if (res == DialogResult.No)
        {
          updateRdbMaster_CheckedState();
        }
        else
        {
          // 現在選択されているラジオボタン更新
          currentRdbMaster = newRadioButton;
          updateRdbMaster_CheckedState();
          // Tagは InputMstFrom.designer.cs に設定されている
          int buttonId = (int)currentRdbMaster.Tag;
          // 1 : rdbKoutei
          // 2 : rdbShitaKbn
          // 3 : rdbSeihinBun
          // 4 : rdbSeihinShu
          switch (buttonId)
          {
            case 1:   // 1 : rdbKoutei
                // ...
                break;
            case 2:   // 2 : rdbShitaKbn
                // ...
                break;
            case 3:   // 3 : rdbSeihinBun
                // ...
                break;
            case 4:   // 4 : rdbSeihinShu
                // ...
                break;
          }
        }
      }
    }
```

`updateRdbMaster_CheckedState()` はそのラジオボタンの状態 (Checked / Unchecked state) を更新するためのメソッド

```cs
private void updateRdbMaster_CheckedState()
    {
        rdbKoutei.Checked = false;
        rdbShitaKbn.Checked = false;
        rdbSeihinBun.Checked = false;
        rdbSeihinShu.Checked = false;

        currentRdbMaster.Checked = true;
    }
```