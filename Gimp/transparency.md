# To make a selection transparent

* Right clic on your layer, Add Alpha Channel
* Select the region to make transparent (color selection, rectangle, add/subtract to selection, ...)
* Delete
