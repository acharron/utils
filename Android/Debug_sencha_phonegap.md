# Debug environement with Sencha and Phonegap

* Install Android SDK and platform-tools
* Install the latest Sencha CMD
* Install NodeJS
* Install Phonegap

```bash
npm install -g phonegap
```

* Build, Serve, Debug

```bash
sencha phonegap init   // Adds "builds native" config to app.json

// ... Edit app.json to have correct platform and id

sencha build native    // Builds the phonegap appli

// ... phonegap/www -> phonegap/www/www.zip

phonegap serve        // From the www folder
```

* Push notifications

```bash
phonegap push --deviceID aaaaAAAA --service fcm --payload "{ \"data\": { \"title\": \"Hello\", \"message\": \"World\"} }"
```

* Logcat

```text
cd C:\aaa\aaa\sdk-tools-windows-12345\platform-tools
.\adb.exe logcat -v brief -s chromium:D
```

## Build script for a phonegap appli

```bash
sencha build native
printf "Build finished.\n"
printf "Starting ZIP to www.zip\n"
rm ./phonegap/www/www.zip
zip -q -r ./phonegap/www/www.zip ./phonegap/www/*
printf "ZIP finished.\n"
printf "Starting phonegap serve...\n"

printf " -- In case of \"[error] listen EADDRINUSE :::3000\" :\n"
printf " -- find and the old server process with (as an admin, cmd.exe)\n"
printf " -- netstat -a -n -o | find \":3000\"  and   tasklist /fi \"pid eq 123456\")\n"

cd ./phonegap/www/
phonegap serve
```