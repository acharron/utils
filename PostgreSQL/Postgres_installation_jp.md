# Postgres DB サーバーのインストール手順書

## サーバー情報

* CentOS 7
* PostgreSQL 10.4

## インストール

([https://www.postgresql.org/download/linux/redhat/](https://www.postgresql.org/download/linux/redhat/) に参考)

```bash
yum install https://download.postgresql.org/pub/repos/yum/10/redhat/rhel-7-x86_64/pgdg-centos10-10-2.noarch.rpm
yum install postgresql10 postgresql10-server postgresql10-contrib
/usr/pgsql-10/bin/postgresql-10-setup initdb
systemctl enable postgresql-10
systemctl start postgresql-10
```

それで、 `postgres` というCentOSユーザーアカウントが作られている。

`postgres` の情報は `/var/lib/pgsql/10/` (ホーム) と　`/usr/pgsql-10/` (binフォルダーなど) にある

### 接続設定

* `/var/lib/pgsql/10/data/pg_hba.conf` を編集して、localhost以外からの接続を許可する。

```text
host      all       all      xxx.xxx.xxx.0/24       md5
// OR
host      all       all      xxx.xxx.xxx.xxx/36     md5
```

* `/var/lib/pgsql/10/data/postgresql.conf` を編集して

```text
#listen_addresses = ''          # what IP address(es) to listen on;
を
listen_addresses = '*'          # what IP address(es) to listen on;
に変更
```

* ファイアウォールの確認と設定を行う

```bash
firewall-cmd --zone=public --list-all          // このコマンドで ports: 5432/tcp が表示されたらそれで終わり。なければ、ポートを開く
firewall-cmd --add-port 5432/tcp --permanent
firewall-cmd --reload
```

## PostgreSQL の操作

### PostgreSQLのコマンドを PATH に追加

デフォールトで `psql` などのPostgreSQLコマンドはCentOSのPATHに追加されていない。  
全てのユーザーのPATHに追加するため、 root で /etc/profile.d にPATH調整の処理を追加する

```bash
echo 'pathmunge /usr/pgsql-10/bin' > /etc/profile.d/postgre_path.sh
```

それで、 `psql`, `createuser` などのPostgreSQLコマンドを簡単に使えます。

### 便利なコマンド

CentOS端末

```bash
// ユーザー変更 (exit で前のユーザーに戻る)
su - root
su - postgres

useradd username     // CentOSユーザーアカウント作成 (!!注意!! : CentOSユーザーとPostgreユーザー(ロール)が違います)
passwd username      // ユーザーのパスワード指定
userdel -r username  // CentOSユーザー削除

// バックアップ作成・読み込み
pg_dump -c --if-exists -E encoding  dbname > dumfile.bak  // デフォールトで普通にテキストファイルでバックアップされている
                                                          // -c --if-exists : 最初に DROP IF EXISTS/CREATE を追加
                                                          // -E encoding    : 文字コード指定 (EUC_JP, UTF8, など)
psql dbname < dumpfile.bak    // バックアップファイルの読み込み (データベースがすでに作られていることを前提)
```

PSQLコマンド

```sql
\dg        // 全てのロール (Postgreユーザー) 一覧
\d         // 現在のDBのリレーション (テーブル、シーケンス、ビュー、...)
\l         // 全てのデータベース一覧
\c dbname  // 別のデータベースに接続

// Postgresユーザー作成
CREATE ROLE username LOGIN;               // ログイン可能のロールを作成 (パスワードを指定しなければならない)
ALTER ROLE username PASSWORD 'passowrd';  // ロールにパスワードを指定
ALTER ROLE username CREATEDB;             // ロールに「DB作成」の権限追加
// 以上のコマンドを一つにまとめれる
CREATE ROLE username LOGIN PASSWORD 'pasword' CREATEDB;

ALTER DATABASE databasename OWNER TO username;  // DBのオーナーを変更
```

## 新規DBの作成

全てのコマンドは PSQL で

```sql
\l     // 全てのDBを確認して、 template0 と template1 のエンコディングを確認

// 新規DBのエンコディングは template0/1 のエンコディングと違う場合、新規テンプレートを作成 (以下、 EUC_JP利用のテンプレート作成)
CREATE DATABASE template_euc OWNER username TEMPLATE template0 ENCODING="EUC_JP" LC_COLLATE="C" LC_CTYPE="C" IS_TEMPLATE true;

// (注意 : template0 を指定するのは大事。デフォールトは template1 の設定をコピーするようになってて、template1のエンコディングを合わせないといけない)


// 新規DB
CREATE DATABASE dbname OWNER username TEMPLATE template;  // TEMPLATEでエンコディングを指定
REVOKE CONNECT ON DATABASE dbname FROM public;            // オーナーや権限のあるユーザーしか dbname へ接続が出来ないように設定する
GRANT CONNECT ON DATABASE dbname TO username;             // username に dbname へアクセス権限を追加
```

## バックアップ時で問題解決

### `pg_logdir_ls doesn't exist`

理由は、 `pg_logdir_ls()` という関数がなかったです。

解決手順 :

* 端末 : `yum install postgresql10-contrib` (すでにあるはずだが、 contrib ファイルがインストールされていることが前提)
* PSQL : `\c dbname`  (問題が起きたDBに接続)
* PSQL : `CREATE EXTENSION adminpack;`  (adminpack という拡張パッケージを現在のDBにインストール)
