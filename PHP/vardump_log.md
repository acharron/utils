# PHP - Log var_dump / print_r to a

When using `var_dump()` or `print_r()`, the output is directly sent to the page. It's possible
to have the output saved in a variable first, to print it in a log file instead of the page.


## print_r

```php
$object = new MyObject();
log(print_r($object, true));
```

If you pass true to the second parameter, the output is returned from the function and thus can be
passed to a log function.


Ref : https://www.php.net/manual/en/function.print-r.php

## var_dump

Use the output buffer functions to capture `var_dump` result and log it

```php
$object = new MyObject();

ob_start();
var_dump($object);
$contents = ob_get_contents();
ob_end_clean();

log($contents);
```

Ref : https://www.php.net/manual/en/function.ob-start.php
