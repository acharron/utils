# PHP - 文字エンコーディングについて

PHP (+ PostgreSQL) の環境で、文字化けの問題が発生しやすい。

そのため、エンコーディングに関する config の変数などをまとめます。

## 参考資料

* https://www.php.net/manual/ja/mbstring.configuration.php
* https://www.php.net/manual/ja/ini.core.php#ini.default-charset


## PHP.ini

例 : `EUC-JP` にしたい場合

`php.ini` の設定ファイルにある変数

```
default_charset = "EUC-JP"
mbstring.detect_order = "EUC-JP"

; PHP 5.6.0 以降のユーザーは、これを空のままにしておいて、代わりに default_charset を設定すべきです。
mbstring.http_input =
mbstring.http_output =
mbstring.internal_encoding =
```

## PostgreSQL データベース

データベースのエンコーディングもPHPのエンコーディングに合わせる必要があります。

例 : `EUC-JP` にしたい場合

```
// psql でログインした後、 Postgre のコマンド

// 作成
CREATE DATABASE database_name WITH TEMPLATE=template0 ENCODING='EUC_JP' LC_COLLATE='C' LC_CTYPE='C';

// テーブルのエンコーディングを確認、以下のような結果が出力
\l


名前          | 所有者    | エンコーディング | 照合順序 | Ctype 
--------------+----------+----------------+----------+--------
database_name | postgres | EUC_JP         | C        | C      
```

[README](../README.md) に PostgreSQL に関する情報も集めてる。

