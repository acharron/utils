# PHP - var_dump / print_r をログに出力

`var_dump()` や `print_r()` を使う場合、画面に出力せずに、変数に記録してログファイルに出力したい場合はこの方法があります。

## print_r

```php
$object = new MyObject();
log(print_r($object, true));
```

2つ目のパラメーターに `true` を渡すと変数になってログに使える

参考 : https://www.php.net/manual/ja/function.print-r.php

## var_dump

アウトプットバーファーのキャプチャーを使って、変数に登録させる

```php
$object = new MyObject();

ob_start();
var_dump($object);
$contents = ob_get_contents();
ob_end_clean();

log($contents);
```

参考 : https://www.php.net/manual/ja/function.ob-start.php
