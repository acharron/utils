# 個人の知恵袋

## 目次



### 言語

#### CSS

* [Bootstrap のようなパネルの作り方](./CSS/css_collapse_panel_jp.md)

#### ExtJS

* TODO [Random utils](./ExtJS/extjs_utils_jp.md)

#### Java

* **Java 11** のJDK (Oracle社) のライセンスが変わりました。有料になってしまいました！ <https://jdk.java.net/11/> OpenJDKを利用するべき (参考 : (EN) <https://blog.joda.org/2018/09/do-not-fall-into-oracles-java-11-trap.html>)
* [NamedParameterStatement : JDBCのSQL Queryにパラメーター名を使えるようにする](./Java/NamedParameterStatement_jp.java)
* [SecurityUtils : パスワード保存と確認の専用クラス (ハッシュ)](./Java/SecurityUtils_jp.java)

#### Javascript

* TODO [Sélectionner des élèments avec un sélecteur CSS](./Javascript/select_with_css_selector.md)
* TODO [Lire un fichier local (fetch)](./Javascript/fetch_local_file_content.md)
* TODO [JavaScript Garden : Trucs à savoir](http://bonsaiden.github.io/JavaScript-Garden/#object.forinloop)

#### PHP

* [PHP 環境で文字エンコーディングに関する](./PHP/character_encoding_jp.md)
* [var_dump / print_r をログに出力](./PHP/vardump_log_jp.md)

#### PostgreSQL

* [PostgreSQL インストール手順](./PostgreSQL/Postgres_installation_jp.md)

#### Python

* <https://www.leoeditor.com/>  Leo IDE : 気になるIDE、別の言語も使える

#### Oracle SQL

* TODO [Alias de table (a.k.a. Ne pas utiliser `AS`)](./Oracle_SQL/table_aliases.md)
* TODO [Oracle 12c scripting](./Oracle_SQL/oracle_12c_script.md)

#### Visual Basic 6

* TODO [Faire une pause dans l'exécution du programme (`Sleep`)](./Visual_Basic_6/sleep.md)

#### Windows Forms

* TODO [Comment utiliser les raccourcis Windows dans `KeyDown()`](./Windows_Forms/using_windows_special_shortcut_keys.md)
* TODO [Radio buttons](./Windows_Forms/radio_buttons.md)



### ツール

#### Gimp

* TODO [Comment ajouter de la transparence](./Gimp/transparency.md)

#### Git

* [Git Config](./Git/gitconfig) : Git Bash用のエイリアスなどのconfigファイル
* [様々な役に立つGitコマンド](./Git/git_commands_jp.md)
* [ログイン情報の保存方法](./Git/asking_for_credentials_jp.md) : aka 「アクセスあるのになぜかクローンが出来ない」となった場合
* [Git Cheat Sheet](./Git/git-cheat-sheet_jp.pdf)

#### GitLab

* TODO [GitLab backups](./GitLab/gitlab_backup.md)

#### Tomcat

* TODO [GitBucket : Lancer Tomcat avec un autre user bloque l'accès à la DB H2 interne](./Tomcat/gitbucket_different_user.md)

#### Visual Studio Code

* Javascript をチェックする

```javascript
// @ts-check

// エラーにならないようにグローバール変数をデバッグ用で、空のオブジェクトで指定
var Ext = {};
```


#### Windows

* [Sysinternals : デバッグ用のWindowsツール](./Windows/sysinternals_jp.md)


#### Tools

* TODO [Everything : Recherche de fichiers et dossiers sur Windows](http://voidtools.com/)
* [フランスのFramasoftが推進するオープンソース・フリーソフトの一覧](https://degooglisons-internet.org/fr/alternatives)



### セキュリティーなど

* (EN) [Ask HN: How do security researchers know where to look for vulnerabilites?](https://news.ycombinator.com/item?id=17176711)
* (EN) [Web Application Penetration Testing Cheat Sheet](./Security/web_app_pen_testing_cheat_sheet.htm) (depuis https://jdow.io/blog/2018/03/18/web-application-penetration-testing-methodology/)



### 面白いプログラマ達の話

* (EN) [Debugging an evil Go runtime bug](https://marcan.st/2017/12/debugging-an-evil-go-runtime-bug/)
* (EN) [QueryStorm: I wandered off and built an IDE](http://blog.querystorm.com/index.php/2018/04/04/whynow/)
* (EN) [Hexagonal Grids](https://www.redblobgames.com/grids/hexagons/)
* (EN) [I’m harvesting credit card numbers and passwords from your site. Here’s how.](https://hackernoon.com/im-harvesting-credit-card-numbers-and-passwords-from-your-site-here-s-how-9a8cb347c5b5)
* (EN) [Largest SimCity ever](https://rumorsontheinternets.org/2010/10/14/magnasanti-the-largest-and-most-terrifying-simcity/)
* (EN) [Factorization diagramms](http://www.datapointed.net/visualizations/math/factorization/animated-diagrams/)
* (EN) [Completely silent computer](https://tp69.wordpress.com/2018/04/17/completely-silent-computer/)
* (EN) [Why did I spend 1.5 months creating a Gameboy emulator?](http://blog.rekawek.eu/2017/02/09/coffee-gb/)
* (EN) [Command-line Tools can be 235x Faster than your Hadoop Cluster](https://adamdrake.com/command-line-tools-can-be-235x-faster-than-your-hadoop-cluster.html)
* (EN) [Excellent article sur "Teaching programming"](http://worrydream.com/LearnableProgramming/)
* (EN) [IPFS et mise à jour de blog](https://ipfs.io/ipns/Qme48wyZ7LaF9gC5693DZyJBtehgaFhaKycESroemD5fNX/post/putting_this_blog_on_ipfs/)
* (EN) IMAP の次のスタンダード ? <https://unencumberedbyfacts.com/2019/01/24/jmap-its-like-imap-but-not-really/>
* フランスパリ際の .gif  [Gif cadeau 14 juillet](https://i.imgur.com/8PtKZrW.gifv)

### 面白いビデオ

* (EN) [6 Things Your Junior Devs Don't Tell You](https://www.youtube.com/watch?v=m6G8f9pZZRM)
* (FR) [Un hacker en train de hacker](https://www.youtube.com/watch?v=qwLcYpoHmkY)
* (FR) <http://tut-tuuut.github.io/conferences/tempete-de-boulettes-geantes>
* (EN) [Holding a program in one's head](http://paulgraham.com/head.html)
* (EN) <https://motherboard.vice.com/en_us/article/43zak3/the-internet-was-built-on-the-free-labor-of-open-source-developers-is-that-sustainable>
* (EN) <https://github.com/charlax/professional-programming>
