# CSS - mix-blend-mode to merge a logo and its background


* https://developer.mozilla.org/en-US/docs/Web/CSS/mix-blend-mode


## Example (from Twitter @pulpxstyle)

* https://twitter.com/pulpxstyle/status/1329575674458238977

```html
<div class="logo">
    <img src="logo.jpg">
</div>
```

```css
.logo {
    mix-blend-mode: multiply;
}
```

![demo](./img_mix_blend_mode.png)
