# CSS - text-overflow

* https://developer.mozilla.org/en-US/docs/Web/CSS/text-overflow

```
text-overflow: clip;
text-overflow: ellipsis;
```

```css
.word {
    width: 5em;
    
    /* BOTH of the following are required for text-overflow */
    white-space: nowrap;
    overflow: hidden;

    text-overflow: ellipsis;
}
```

gives

```
 -------------
|aaaaaaaaaa...|
 -------------
```
