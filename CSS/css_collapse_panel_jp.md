# CSS - Bootstrap のような collapse パネル

<https://getbootstrap.com/docs/4.3/components/collapse/> のように、スムーズな表示・非表示を
`transition` というCSS属性で作る

必要だけの HTML/Javascript/CSS　を紹介

```html
<div class="RadioCollapsePanel">
    <input type="radio">
</div>

<div class="CollapsePanelContent">
    <div>aaa</div>
</div>


<script>
onRadioCheck() {
    radioPanel.removeCls("collapsed");
    contents.addCls("shown");

    if (adjustMaxHeight === true) {
        // アニメーションが終わった後、"100%" でなくて実際の高さを指定する
        // なぜかというと、 "100%" のままであれば、非表示のアニメーションが遅く感じるから
        setTimeout(function() {
            var newMaxHeight = contents.dom.clientHeight + 5;
            contents.dom.maxHeight = max + "px";
        }, 450);
    }
}

onRadioUncheck() {
    radioPanel.addCls("collapsed");
    contents.removeCls("shown");
    contents.el.dom.style.maxHeight = null;
}
</script>
```

```css
.RadioCollapsePanel,
.CollapsePanelContent {
    border: solid 1px #000;
    border-collapse: collapse;
}
.RadioCollapsePanel {
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    min-height: 3em;
}


.RadioCollapsePanel.collapsed,
.CollapsePanelContent {
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
}
 .CollapsePanelContent {
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.35s ease-in-out;    /* !! ここが大事 !! */
}
 .CollapsePanelContent.shown {
    max-height: 100%;
}
```
