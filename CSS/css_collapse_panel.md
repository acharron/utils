# CSS - A collapsible panel (a la Bootstrap)

Making a collapsible panel by using the `transition` property.

```html
<div class="RadioCollapsePanel">
    <input type="radio">
</div>

<div class="CollapsePanelContent">
    <div>aaa</div>
</div>


<script>
onRadioCheck() {
    radioPanel.removeCls("collapsed");
    contents.addCls("shown");

    if (adjustMaxHeight === true) {
        // We set the max-height to a more "real-life value", because with "100%"
        //   the closing animation feels too long than necessary
        setTimeout(function() {
            var newMaxHeight = contents.dom.clientHeight + 5;
            contents.dom.maxHeight = max + "px";
        }, 450);
    }
}

onRadioUncheck() {
    radioPanel.addCls("collapsed");
    contents.removeCls("shown");
    contents.el.dom.style.maxHeight = null;
}
</script>
```

```css
.RadioCollapsePanel,
.CollapsePanelContent {
    border: solid 1px #000;
    border-collapse: collapse;
}
.RadioCollapsePanel {
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    min-height: 3em;
}


.RadioCollapsePanel.collapsed,
.CollapsePanelContent {
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
}
 .CollapsePanelContent {
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.35s ease-in-out;    /* !!!  That's the part */
}
 .CollapsePanelContent.shown {
    max-height: 100%;
}
```
