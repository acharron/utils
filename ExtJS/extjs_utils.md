# ExtJS Utils

* [Display events](#1)
* [Delayed task](#2) 
* [Select in Dataview](#3)
* [`build testing` : Prevent code minifying](#4)


##### 1

## Display all events concerning an object

```javascript
Ext.util.Observable.capture(myObj, function(evname) {console.log(evname, arguments);});
```


##### 2

## Delayed task

```javascript
Ext.create('Ext.util.DelayedTask', function () {
    ...
}).delay(100);


// Javascript native
setTimeout(fn, timeout);
```

##### 3

## Select an item in Ext.dataview.DataView

If you think `setSelected(collection)` is the right way, you're wrong.
The correct way to select something in a dataview is to grab the underlying
model and select the record from there.

To grab that model, you need to use `getSelectable()`

```javascript
var myRecord = store.query("aaa", "123", false, true, true).first();

if (myRecord) dataview.getSelectable().select(myRecord);
```

* [`select()`](https://docs.sencha.com/extjs/6.6.0/modern/Ext.dataview.selection.Model.html#method-select)
* [`selectable`](https://docs.sencha.com/extjs/6.6.0/modern/Ext.dataview.DataView.html#cfg-selectable)
* [Stack Overflow answer](https://stackoverflow.com/questions/49451601/deselect-list-method-in-extjs-6-5-3)



##### 4

## Prevent code minifying when building the app

When building without options, the default behaviour is to minify the code
before copying it to `phonegap/www`. However, that makes code debugging
difficult, with error messages like 
```
TypeError: c is not a constructor
```

By using `sencha build android testing`, the code is not minified, and
the error messages become clear.

```
Error: [Ext.createByAlias] Unrecognized alias: data.validator.email
Error: [Ext.createByAlias] Unrecognized alias: data.validator.range
```

