# Comment faire une pause dans le fil d'exécution

## Sleep()

* Créer un nouveau module
* Ajouter le code ci-dessous
* Appeler `Sleep()` depuis notre form

```visualbasic
' In module
Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

' In form method
Call Sleep(1000)
```
