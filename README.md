# Répertoire contenant plein de petites choses utiles à retenir

[日本語版](./README_jp.md)


## Table des matières


### Langages

#### Android

* [Debug Sencha Phonegap](./Android/Debug_sencha_phonegap.md)

#### CSS

* [Faire un panel qui s'ouvre et se ferme à la Bootstrap](./CSS/css_collapse_panel.md)
* [CSS mix-blend-mode : Mix logo and background](./CSS/css_mix_blend_mode.md)
* [text-overflow : Cut a word too long with "..."](./CSS/css_text_overflow.md)

#### ExtJS

* [Random utils](./ExtJS/extjs_utils.md)

#### Java

* **Java 11** JDK (by Oracle) is a commercial license!! Use <https://jdk.java.net/11/> instead (via <https://blog.joda.org/2018/09/do-not-fall-into-oracles-java-11-trap.html>)
* [NamedParameterStatement : Helper class pour utiliser JDBC avec des paramètres](./Java/NamedParameterStatement.java)
* [SecurityUtils : Gestion de mots de passe (hashing)](./Java/SecurityUtils.java)

#### Javascript

* [Sélectionner des élèments avec un sélecteur CSS](./Javascript/select_with_css_selector.md)
* [Lire un fichier local (fetch)](./Javascript/fetch_local_file_content.md)
* [JavaScript Garden : Trucs à savoir](http://bonsaiden.github.io/JavaScript-Garden/#object.forinloop)

#### PHP

* TODO [PHP 環境で文字エンコーディングに関する](./PHP/character_encoding.md)
* [Log var_dump / print_r to a file](./PHP/vardump_log.md)

#### PostgreSQL

* TODO [PostgreSQL install](./PostgreSQL/Postgres_installation.md)

#### Python

* <https://www.leoeditor.com/>  Leo IDE : Unusual editor, can be used for other languages

#### Oracle SQL

* [Alias de table (a.k.a. Ne pas utiliser `AS`)](./Oracle_SQL/table_aliases.md)
* [Oracle 12c scripting](./Oracle_SQL/oracle_12c_script.md)

#### Visual Basic 6

* [Faire une pause dans l'exécution du programme (`Sleep`)](./Visual_Basic_6/sleep.md)

#### Windows Forms

* [Comment utiliser les raccourcis Windows dans `KeyDown()`](./Windows_Forms/using_windows_special_shortcut_keys.md)
* [Radio buttons](./Windows_Forms/radio_buttons.md)



### Outils autour de la programmation

#### CLI

* [Awesome console services](./CLI/awesome_cli_services.md) (depuis https://github.com/chubin/awesome-console-services)
* [Byobu : termianl multiplexer (tiler)](http://www.byobu.co/)
* [How to do things safely in Bash](./CLI/how_to_do_things_safely_in_bash.md) (depuis https://github.com/anordal/shellharden/blob/master/how_to_do_things_safely_in_bash.md)
* [tmux & screen cheat-sheet](./CLI/tmux_screen_cheat_sheet.htm) (depuis http://www.dayid.org/comp/tm.html)
* [Userland - a book about cli for humans](https://p1k3.com/userland-book/)
* <https://linuxfr.org/users/siltaar/journaux/ligne-de-commande-les-20-memos-d-un-autodidacte>
* [Linux networking tools cheatsheet (by Julia Evens)](./CLI/linux_net_tools.jpg)

#### Gimp

* [Comment ajouter de la transparence](./Gimp/transparency.md)

#### Git

* [Git Config](./Git/gitconfig)
* [Commandes Git en vrac](./Git/git_commands.md)
* [Asking for credentials (a.k.a. I can't clone for no reason)](./Git/asking_for_credentials.md)
* [Git Cheat Sheet](./Git/Git-Cheat-Sheet.png)

#### GitLab

* [GitLab backups](./GitLab/gitlab_backup.md)

#### Tomcat

* [GitBucket : Lancer Tomcat avec un autre user bloque l'accès à la DB H2 interne](./Tomcat/gitbucket_different_user.md)

#### Visual Studio Code


#### Windows

* [Sysinternals : Essential windows tools for debugging](./Windows/sysinternals.md)

#### Tools

* [Everything : Recherche de fichiers et dossiers sur Windows](http://voidtools.com/)
* <https://degooglisons-internet.org/fr/alternatives>
* Let's Encrypt + Other free CAs : https://scotthelme.co.uk/introducing-another-free-ca-as-an-alternative-to-lets-encrypt/
  * Let's Encrypt
  * Buypass
  * ZeroSSL
  * <https://github.com/acmesh-official/acme.sh>


#### Ubuntu

* [Importants .deb to install](./Ubuntu/importants_deb.md)

### Sécurité, etc

* [Ask HN: How do security researchers know where to look for vulnerabilites?](https://news.ycombinator.com/item?id=17176711)
* [Web Application Penetration Testing Cheat Sheet](./Security/web_app_pen_testing_cheat_sheet.htm) (depuis https://jdow.io/blog/2018/03/18/web-application-penetration-testing-methodology/)



### Récits épiques de programmeurs badass

* [Debugging an evil Go runtime bug](https://marcan.st/2017/12/debugging-an-evil-go-runtime-bug/)
* [QueryStorm: I wandered off and built an IDE](http://blog.querystorm.com/index.php/2018/04/04/whynow/)
* [Hexagonal Grids](https://www.redblobgames.com/grids/hexagons/)
* [I’m harvesting credit card numbers and passwords from your site. Here’s how.](https://hackernoon.com/im-harvesting-credit-card-numbers-and-passwords-from-your-site-here-s-how-9a8cb347c5b5)
* [Largest SimCity ever](https://rumorsontheinternets.org/2010/10/14/magnasanti-the-largest-and-most-terrifying-simcity/)
* [Factorization diagramms](http://www.datapointed.net/visualizations/math/factorization/animated-diagrams/)
* [Completely silent computer](https://tp69.wordpress.com/2018/04/17/completely-silent-computer/)
* [Why did I spend 1.5 months creating a Gameboy emulator?](http://blog.rekawek.eu/2017/02/09/coffee-gb/)
* [Command-line Tools can be 235x Faster than your Hadoop Cluster](https://adamdrake.com/command-line-tools-can-be-235x-faster-than-your-hadoop-cluster.html)
* [Excellent article sur "Teaching programming"](http://worrydream.com/LearnableProgramming/)
* [IPFS et mise à jour de blog](https://ipfs.io/ipns/Qme48wyZ7LaF9gC5693DZyJBtehgaFhaKycESroemD5fNX/post/putting_this_blog_on_ipfs/)
* <https://unencumberedbyfacts.com/2019/01/24/jmap-its-like-imap-but-not-really/>
* [Gif cadeau 14 juillet](https://i.imgur.com/8PtKZrW.gifv)



### Vidéos / Conférences cools

* [6 Things Your Junior Devs Don't Tell You](https://www.youtube.com/watch?v=m6G8f9pZZRM)
* [Un hacker en train de hacker](https://www.youtube.com/watch?v=qwLcYpoHmkY)
* <http://tut-tuuut.github.io/conferences/tempete-de-boulettes-geantes>
* [Holding a program in one's head](http://paulgraham.com/head.html)
* <https://motherboard.vice.com/en_us/article/43zak3/the-internet-was-built-on-the-free-labor-of-open-source-developers-is-that-sustainable>
* <https://github.com/charlax/professional-programming>

### Autres

* [Demande Valeur Fonciere](https://app.dvf.etalab.gouv.fr/)
